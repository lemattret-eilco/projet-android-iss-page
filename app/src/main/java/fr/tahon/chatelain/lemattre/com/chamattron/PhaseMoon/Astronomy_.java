package fr.tahon.chatelain.lemattre.com.chamattron.PhaseMoon;

import java.util.HashMap;
import java.util.Map;

public class Astronomy_ {
    private String sunrise;
    private String sunset;
    private String moonrise;
    private String moonset;
    private Double moonPhase;
    private String moonPhaseDesc;
    private String iconName;
    private String city;
    private Double latitude;
    private Double longitude;
    private String utcTime;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getSunrise() {
        return sunrise;
    }

    public void setSunrise(String sunrise) {
        this.sunrise = sunrise;
    }

    public String getSunset() {
        return sunset;
    }

    public void setSunset(String sunset) {
        this.sunset = sunset;
    }

    public String getMoonrise() {
        return moonrise;
    }

    public void setMoonrise(String moonrise) {
        this.moonrise = moonrise;
    }

    public String getMoonset() {
        return moonset;
    }

    public void setMoonset(String moonset) {
        this.moonset = moonset;
    }

    public Double getMoonPhase() {
        return moonPhase;
    }

    public void setMoonPhase(Double moonPhase) {
        this.moonPhase = moonPhase;
    }

    public String getMoonPhaseDesc() {
        return moonPhaseDesc;
    }

    public void setMoonPhaseDesc(String moonPhaseDesc) {
        this.moonPhaseDesc = moonPhaseDesc;
    }

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getUtcTime() {
        return utcTime;
    }

    public void setUtcTime(String utcTime) {
        this.utcTime = utcTime;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
