package fr.tahon.chatelain.lemattre.com.chamattron.Activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import fr.tahon.chatelain.lemattre.com.chamattron.Iss.Crew;
import fr.tahon.chatelain.lemattre.com.chamattron.R;
import fr.tahon.chatelain.lemattre.com.chamattron.RetrofitService.RetrofitService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static fr.tahon.chatelain.lemattre.com.chamattron.RetrofitService.RetrofitService.ENDPOINT;


public class IssCrewActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private TextView nb;
    private TextView capt1;
    private TextView pil1;
    private TextView sec1;
    private TextView capt2;
    private TextView pil2;
    private TextView sec2;
    private TextView titre;
    private DrawerLayout mDrawer;
    private NavigationView navigationView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboard_crew);


        Toolbar toolbar3 = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar3);
        getSupportActionBar().setTitle("Equipage Iss");
        titre = (TextView) findViewById(R.id.ISSCrewTitle);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "ARDELANEY.ttf");
        titre.setTypeface(typeface);

        nb = (TextView) findViewById(R.id.CrewNumber);

        capt1 = (TextView) findViewById(R.id.Captain);
        pil1 = (TextView) findViewById(R.id.Pilote);
        sec1 = (TextView) findViewById(R.id.Second);

        capt2 = (TextView) findViewById(R.id.Captain2);
        pil2 = (TextView) findViewById(R.id.Pilote2);
        sec2 = (TextView) findViewById(R.id.Second2);

        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar3, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        //new JsonTask().execute("Url address here");

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        retrofit.create(RetrofitService.class);

        RetrofitService service = retrofit.create(RetrofitService.class);


        service.listExample2().enqueue(new Callback<Crew>() {
            @Override
            public void onResponse(Call<Crew> call, Response<Crew> response) {

                int nombre = response.body().getNumber();
                nb.setText(response.body().getNumber().toString());

                capt1.setText(response.body().getPeople().get(0).getName().toString());
                pil1.setText(response.body().getPeople().get(1).getName().toString());
                sec1.setText(response.body().getPeople().get(2).getName().toString());

                if (nombre >3) {
                    if (response.body().getPeople().get(3).getName().toString() != null && response.body().getPeople().get(4).getName().toString() != null && response.body().getPeople().get(5).getName().toString() != null) {
                        capt2.setText(response.body().getPeople().get(3).getName().toString());
                        pil2.setText(response.body().getPeople().get(4).getName().toString());
                        sec2.setText(response.body().getPeople().get(5).getName().toString());
                    } else {
                        capt2.setText("Pas de 2nde équipe");
                        pil2.setText("Pas de 2nde équipe");
                        sec2.setText("Pas de 2nde équipe");
                    }
                }
                else {
                    capt2.setText("Pas de 2nde équipe");
                    pil2.setText("Pas de 2nde équipe");
                    sec2.setText("Pas de 2nde équipe");
                }

            }

            @Override
            public void onFailure(Call<Crew> call, Throwable t) {
                Toast.makeText(IssCrewActivity.this, "ERROR", Toast.LENGTH_SHORT).show();
            }
        });



    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();

        if (id == R.id.nav_pos) {
            change1();
            finish();
        } else if (id == R.id.nav_crew) {
            change2();
        } else if(id == R.id.nav_home){
            change3();
            finish();
        } else if(id == R.id.nav_roadster){
            change4();
            finish();
        }else if(id == R.id.nav_launch){
            change5();
            finish();
        }else if(id == R.id.nav_data){
            change6();
            finish();
        }else if(id == R.id.nav_logout){
            change7();
            finish();
        }else if(id == R.id.nav_phasemoon){
            change8();
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void change1(){
        Intent intent  = new Intent(this, IssPositionActivity.class);
        startActivity(intent);
        //Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }
    public void change2(){
       /* Intent intent  = new Intent(this, IssCrewActivity.class);
        startActivity(intent);*/
        Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }

    public void change3(){
        Intent intent = new  Intent(this, MenuActivity.class);
        startActivity(intent);
        //Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }

    public void change4(){
        Intent intent = new  Intent(this, RoadsterInfosActivity.class);
        startActivity(intent);
        //Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }

    public void change5(){
        Intent intent = new Intent(this, NextLaunchActivity.class);
        startActivity(intent);
        //Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }

    public void change6(){
        Intent intent = new Intent(this, CompDataActivity.class);
        startActivity(intent);
        //Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }
    public void change7(){
        Intent intent = new Intent(this, ConnexionActivity.class);
        startActivity(intent);
    }
    public void change8(){
        Intent intent = new Intent(this, PhaseMoonActivity.class);
        startActivity(intent);
    }
}
