package fr.tahon.chatelain.lemattre.com.chamattron.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import fr.tahon.chatelain.lemattre.com.chamattron.R;


public class ConnexionActivity extends AppCompatActivity {

    private EditText id;
    private EditText mdp;
    private Button connexion;
    private Button inscription;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connexion_activity);

        this.id = (EditText) findViewById(R.id.identifiant);
        this.mdp = (EditText) findViewById(R.id.password);
        this.connexion = (Button) findViewById(R.id.connexion);
        this.inscription = (Button) findViewById(R.id.inscription);

        this.connexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Activity_menu();

            }
        });

        this.inscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Activity_inscription();
            }
        });

    }

    public void Activity_menu(){
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
    }

    public void Activity_inscription(){
        Intent intent = new Intent(this, InscritpionActivity.class);
        startActivity(intent);
    }
}
