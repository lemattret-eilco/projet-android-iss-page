package fr.tahon.chatelain.lemattre.com.chamattron.SpaceX;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Links {

    private String mission_patch;


    public String getMissionPatch() {
        return mission_patch;
    }

    public void setMissionPatch(String missionPatch) {
        this.mission_patch = missionPatch;
    }


}