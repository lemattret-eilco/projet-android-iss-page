package fr.tahon.chatelain.lemattre.com.chamattron.Iss;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Crew {

    private List<Person> people = null;
    private String message;
    private Integer number;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public List<Person> getPeople() {
        return people;
    }

    public void setPeople(List<Person> people) {
        this.people = people;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}