package fr.tahon.chatelain.lemattre.com.chamattron.Activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import fr.tahon.chatelain.lemattre.com.chamattron.PhaseMoon.Moon;
import fr.tahon.chatelain.lemattre.com.chamattron.R;
import fr.tahon.chatelain.lemattre.com.chamattron.RetrofitService.RetrofitService3;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PhaseMoonActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private TextView phasemoon;
    private TextView titre;
    private TextView Ville;
    private TextView Heure;
    private TextView Pays;
    private TextView Etat;
    private TextView dateH;
    private EditText changement;
    private Button btnVille;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phase_moon);
        Toolbar toolbar7 = (Toolbar) findViewById(R.id.toolbar);
        changement = (EditText) findViewById(R.id.newville);
        btnVille = (Button) findViewById(R.id.btnchange);
        setSupportActionBar(toolbar7);
        getSupportActionBar().setTitle("Phase de la lune");
        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar7, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RetrofitService3.ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        retrofit.create(RetrofitService3.class);
            btnVille.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String city = changement.getText().toString();
                    if (!city.isEmpty()) {
                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl(RetrofitService3.ENDPOINT)
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();

                        retrofit.create(RetrofitService3.class);
                        RetrofitService3 service = retrofit.create(RetrofitService3.class);
                        service.listExample("json", "W4tghLWiJWarfZboFOFS", "Yzbsi_Vm7_UGFTogsRa65Q", "forecast_astronomy", city).enqueue(new Callback<Moon>() {

                            @Override
                            public void onResponse(Call<Moon> call, Response<Moon> response) {
                                double pourcentage = response.body().getAstronomy().getAstronomy().get(0).getMoonPhase() * 100;
                                phasemoon.setText(String.valueOf(pourcentage) + " %");
                                Ville.setText(response.body().getAstronomy().getCity().toString());
                                Pays.setText(response.body().getAstronomy().getCountry().toString());
                                Etat.setText(response.body().getAstronomy().getState());
                                Heure.setText("UTC "+response.body().getAstronomy().getTimezone().toString());
                                dateH.setText(response.body().getFeedCreation());

                            }

                            @Override
                            public void onFailure(Call<Moon> call, Throwable t) {
                                Toast.makeText(PhaseMoonActivity.this, "ERROR", Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                    else {
                        Toast.makeText(getApplicationContext(), "Champ vide", Toast.LENGTH_SHORT).show();
                    }
                }
            });


        RetrofitService3 service = retrofit.create(RetrofitService3.class);
        /*format = "json"
        app_id = "W4tghLWiJWarfZboFOFS"
        app_code = "Yzbsi_Vm7_UGFTogsRa65Q"
        product = "forecast_astronomy"
        name = "Calais"*/
        service.listExample("json", "W4tghLWiJWarfZboFOFS","Yzbsi_Vm7_UGFTogsRa65Q","forecast_astronomy","Calais").enqueue(new Callback<Moon>() {

            @Override
            public void onResponse(Call<Moon> call, Response<Moon> response) {
                double pourcentage = response.body().getAstronomy().getAstronomy().get(0).getMoonPhase()*100;
                phasemoon.setText(String.valueOf(pourcentage)+" %");
                Ville.setText(response.body().getAstronomy().getCity().toString());
                Pays.setText(response.body().getAstronomy().getCountry().toString());
                Etat.setText(response.body().getAstronomy().getState());
                Heure.setText("UTC "+response.body().getAstronomy().getTimezone().toString());
                dateH.setText(response.body().getFeedCreation());

            }

            @Override
            public void onFailure(Call<Moon> call, Throwable t) {
                Toast.makeText(PhaseMoonActivity.this, "ERROR", Toast.LENGTH_SHORT).show();
            }
        });



        titre = (TextView) findViewById(R.id.moontitre);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "ARDELANEY.ttf");
        titre.setTypeface(typeface);
        phasemoon = (TextView) findViewById(R.id.PhaseLune);
        Ville=(TextView) findViewById(R.id.Rville);
        Heure=(TextView) findViewById(R.id.HeureL);
        Pays=(TextView) findViewById(R.id.PaysL);
        Etat=(TextView) findViewById(R.id.Retat);
        dateH=(TextView) findViewById(R.id.RdateH);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();

        if (id == R.id.nav_pos) {
            change1();
            finish();
        } else if (id == R.id.nav_crew) {
            change2();
            finish();
        } else if(id == R.id.nav_home){
            change3();
            finish();
        } else if(id == R.id.nav_roadster){
            change4();
        }else if(id == R.id.nav_launch){
            change5();
            finish();
        }else if(id == R.id.nav_data){
            change6();
            finish();
        } else if (id == R.id.nav_logout){
            change7();
            finish();
        } else if (id == R.id.nav_phasemoon){
            change8();
            finish();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void change1(){
        Intent intent  = new Intent(this, IssPositionActivity.class);
        startActivity(intent);
        //Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }
    public void change2(){
        Intent intent  = new Intent(this, IssCrewActivity.class);
        startActivity(intent);
    }

    public void change3(){
        Intent intent = new  Intent(this, MenuActivity.class);
        startActivity(intent);
        //Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }

    public void change4(){
        Intent intent = new  Intent(this, RoadsterInfosActivity.class);
        startActivity(intent);
        //Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }

    public void change5(){
        Intent intent = new  Intent(this, NextLaunchActivity.class);
        startActivity(intent);
        //Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }

    public void change6(){
        Intent intent = new  Intent(this, CompDataActivity.class);
        startActivity(intent);
        //Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }
    public void change7(){
        Intent intent = new Intent(this, ConnexionActivity.class);
        startActivity(intent);
    }
    public void change8(){
        /*Intent intent = new Intent(this, PhaseMoonActivity.class);
        startActivity(intent);*/
        Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }
}
