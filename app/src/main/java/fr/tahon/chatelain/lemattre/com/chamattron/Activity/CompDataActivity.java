package fr.tahon.chatelain.lemattre.com.chamattron.Activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import fr.tahon.chatelain.lemattre.com.chamattron.SpaceX.InfosComp;
import fr.tahon.chatelain.lemattre.com.chamattron.R;
import fr.tahon.chatelain.lemattre.com.chamattron.RetrofitService.RertrofitService2;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CompDataActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private TextView found;
    private TextView datefound;
    private TextView pdg;
    private TextView de;
    private TextView dtp;
    private TextView ad;
    private TextView vil;
    private TextView et;
    private TextView nbemp;
    private TextView nbv;
    private TextView nsl;
    private TextView nst;
    private TextView titre;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comp_data);
        Toolbar toolbar6 = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar6);
        getSupportActionBar().setTitle("Space X infos");
        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar6, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        titre = (TextView) findViewById(R.id.cdt);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "ARDELANEY.ttf");
        titre.setTypeface(typeface);
        found = (TextView) findViewById(R.id.cdf1);
        datefound = (TextView) findViewById(R.id.cdfd1);
        pdg = (TextView) findViewById(R.id.cdceo1);
        de = (TextView) findViewById(R.id.cdcoo1);
        dtp = (TextView) findViewById(R.id.cdctop1);
        ad = (TextView) findViewById(R.id.cda1);
        vil = (TextView) findViewById(R.id.cdc1);
        et = (TextView) findViewById(R.id.cds1);
        nbemp = (TextView) findViewById(R.id.cdne1);
        nbv = (TextView) findViewById(R.id.cdnv1);
        nsl = (TextView) findViewById(R.id.cdlsl1);
        nst = (TextView) findViewById(R.id.cdts1);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RertrofitService2.ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        retrofit.create(RertrofitService2.class);

        RertrofitService2 service = retrofit.create(RertrofitService2.class);


        service.listExample3().enqueue(new Callback<InfosComp>() {
            @Override
            public void onResponse(Call<InfosComp> call, Response<InfosComp> response) {
                found.setText(response.body().getFounder());
                datefound.setText(response.body().getFounded().toString());
                pdg.setText(response.body().getCeo().toString());
                de.setText(response.body().getCoo().toString());
                dtp.setText(response.body().getCtoPropulsion().toString());
                ad.setText(response.body().getHeadquarters().getAddress().toString());
                vil.setText(response.body().getHeadquarters().getCity().toString());
                et.setText(response.body().getHeadquarters().getState().toString());
                nbemp.setText(response.body().getEmployees().toString());
                nbv.setText(response.body().getVehicles().toString());
                nsl.setText(response.body().getLaunchSites().toString());
                nst.setText(response.body().getTestSites().toString());

            }

            @Override
            public void onFailure(Call<InfosComp> call, Throwable t) {
                Toast.makeText(CompDataActivity.this, "ERROR", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();

        if (id == R.id.nav_pos) {
            change1();
            finish();
        } else if (id == R.id.nav_crew) {
            change2();
            finish();
        } else if (id == R.id.nav_home){
            change3();
            finish();
        }else if(id == R.id.nav_roadster){
            change4();
            finish();
        }
        else if(id == R.id.nav_launch){
            change5();
            finish();
        } else if(id == R.id.nav_data){
            change6();
        }else if(id == R.id.nav_logout){
            change7();
            finish();
        }else if(id == R.id.nav_phasemoon){
            change8();
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void change1(){
        Intent intent  = new Intent(this, IssPositionActivity.class);
        startActivity(intent);
        //Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }
    public void change2(){
        Intent intent  = new Intent(this, IssCrewActivity.class);
        startActivity(intent);
    }

    public void change3(){
        Intent intent = new  Intent(this, MenuActivity.class);
        startActivity(intent);
        //Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }

    public void change4(){
        Intent intent = new  Intent(this, RoadsterInfosActivity.class);
        startActivity(intent);
        //Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }

    public void change5(){
        Intent intent = new Intent(this, NextLaunchActivity.class);
        startActivity(intent);
        //Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }

    public void change6(){
        /*Intent intent = new Intent(this, CompDataActivity.class);
        startActivity(intent);*/
        Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }
    public void change7(){
        Intent intent = new Intent(this, ConnexionActivity.class);
        startActivity(intent);
    }
    public void change8(){
        Intent intent = new Intent(this, PhaseMoonActivity.class);
        startActivity(intent);
    }

}
