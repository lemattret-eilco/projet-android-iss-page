package fr.tahon.chatelain.lemattre.com.chamattron.SpaceX;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Roadster {

    private String name;
    private String launch_date_utc;
    private Integer launchDateUnix;
    private Integer launch_mass_kg;
    private Integer launchMassLbs;
    private Integer noradId;
    private Double epochJd;
    private String orbit_type;
    private Double apoapsisAu;
    private Double periapsisAu;
    private Double semiMajorAxisAu;
    private Double eccentricity;
    private Double inclination;
    private Double longitude;
    private Double periapsisArg;
    private Double periodDays;
    private Double speedKph;
    private Double speed_mph;
    private Double earth_distance_km;
    private Double earthDistanceMi;
    private Double mars_distance_km;
    private Double marsDistanceMi;
    private List<String> flickr_images = null;
    private String wikipedia;
    private String details;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLaunchDateUtc() {
        return launch_date_utc;
    }

    public void setLaunchDateUtc(String launchDateUtc) {
        this.launch_date_utc = launchDateUtc;
    }

    public Integer getLaunchDateUnix() {
        return launchDateUnix;
    }

    public void setLaunchDateUnix(Integer launchDateUnix) {
        this.launchDateUnix = launchDateUnix;
    }

    public Integer getLaunchMassKg() {
        return launch_mass_kg;
    }

    public void setLaunchMassKg(Integer launchMassKg) {
        this.launch_mass_kg = launchMassKg;
    }

    public Integer getLaunchMassLbs() {
        return launchMassLbs;
    }

    public void setLaunchMassLbs(Integer launchMassLbs) {
        this.launchMassLbs = launchMassLbs;
    }

    public Integer getNoradId() {
        return noradId;
    }

    public void setNoradId(Integer noradId) {
        this.noradId = noradId;
    }

    public Double getEpochJd() {
        return epochJd;
    }

    public void setEpochJd(Double epochJd) {
        this.epochJd = epochJd;
    }

    public String getOrbitType() {
        return orbit_type;
    }

    public void setOrbitType(String orbitType) {
        this.orbit_type = orbitType;
    }

    public Double getApoapsisAu() {
        return apoapsisAu;
    }

    public void setApoapsisAu(Double apoapsisAu) {
        this.apoapsisAu = apoapsisAu;
    }

    public Double getPeriapsisAu() {
        return periapsisAu;
    }

    public void setPeriapsisAu(Double periapsisAu) {
        this.periapsisAu = periapsisAu;
    }

    public Double getSemiMajorAxisAu() {
        return semiMajorAxisAu;
    }

    public void setSemiMajorAxisAu(Double semiMajorAxisAu) {
        this.semiMajorAxisAu = semiMajorAxisAu;
    }

    public Double getEccentricity() {
        return eccentricity;
    }

    public void setEccentricity(Double eccentricity) {
        this.eccentricity = eccentricity;
    }

    public Double getInclination() {
        return inclination;
    }

    public void setInclination(Double inclination) {
        this.inclination = inclination;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getPeriapsisArg() {
        return periapsisArg;
    }

    public void setPeriapsisArg(Double periapsisArg) {
        this.periapsisArg = periapsisArg;
    }

    public Double getPeriodDays() {
        return periodDays;
    }

    public void setPeriodDays(Double periodDays) {
        this.periodDays = periodDays;
    }

    public Double getSpeedKph() {
        return speedKph;
    }

    public void setSpeedKph(Double speedKph) {
        this.speedKph = speedKph;
    }

    public Double getSpeedMph() {
        return speed_mph;
    }

    public void setSpeedMph(Double speedMph) {
        this.speed_mph = speedMph;
    }

    public Double getEarthDistanceKm() {
        return earth_distance_km;
    }

    public void setEarthDistanceKm(Double earthDistanceKm) {
        this.earth_distance_km = earthDistanceKm;
    }

    public Double getEarthDistanceMi() {
        return earthDistanceMi;
    }

    public void setEarthDistanceMi(Double earthDistanceMi) {
        this.earthDistanceMi = earthDistanceMi;
    }

    public Double getMarsDistanceKm() {
        return mars_distance_km;
    }

    public void setMarsDistanceKm(Double marsDistanceKm) {
        this.mars_distance_km = marsDistanceKm;
    }

    public Double getMarsDistanceMi() {
        return marsDistanceMi;
    }

    public void setMarsDistanceMi(Double marsDistanceMi) {
        this.marsDistanceMi = marsDistanceMi;
    }

    public List<String> getFlickrImages() {
        return flickr_images;
    }

    public void setFlickrImages(List<String> flickrImages) {
        this.flickr_images = flickrImages;
    }

    public String getWikipedia() {
        return wikipedia;
    }

    public void setWikipedia(String wikipedia) {
        this.wikipedia = wikipedia;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}