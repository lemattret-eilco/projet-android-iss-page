package fr.tahon.chatelain.lemattre.com.chamattron.Activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;

import fr.tahon.chatelain.lemattre.com.chamattron.SpaceX.NextLaunch;
import fr.tahon.chatelain.lemattre.com.chamattron.R;
import fr.tahon.chatelain.lemattre.com.chamattron.RetrofitService.RertrofitService2;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;



public class NextLaunchActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private TextView num;
    private TextView misnam;
    private TextView launchdate;
    private TextView rockid;
    private TextView sitnam;
    private TextView titre;
    private ImageView mispatch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prochain_lancement);


        Typeface typeface = Typeface.createFromAsset(getAssets(),"ARDELANEY.ttf");
        titre = (TextView) findViewById(R.id.plt);
        titre.setTypeface(typeface);
        num = (TextView) findViewById(R.id.pln1);
        misnam = (TextView) findViewById(R.id.plmn1);
        launchdate = (TextView) findViewById(R.id.plldu1);
        rockid = (TextView) findViewById(R.id.plri1);
        sitnam = (TextView) findViewById(R.id.plsn1);
        mispatch = (ImageView) findViewById(R.id.plmp);
        Toolbar toolbar5 = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar5);
        getSupportActionBar().setTitle("Prochaine mission");

        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar5, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RertrofitService2.ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        retrofit.create(RertrofitService2.class);

        RertrofitService2 service = retrofit.create(RertrofitService2.class);

        service.listExample2().enqueue(new Callback<NextLaunch>() {
            @Override
            public void onResponse(Call<NextLaunch> call, Response<NextLaunch> response) {
                num.setText(response.body().getFlightNumber().toString());
                misnam.setText(response.body().getMissionName());
                launchdate.setText(response.body().getLaunchDateUtc());
                rockid.setText(response.body().getRocket().getRocketId());
                sitnam.setText(response.body().getLaunchSite().getSiteName());
                Glide.with(NextLaunchActivity.this).load(response.body().getLinks().getMissionPatch()).into(mispatch);

            }

            @Override
            public void onFailure(Call<NextLaunch> call, Throwable t) {
                Toast.makeText(NextLaunchActivity.this, "ERROR", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();

        if (id == R.id.nav_pos) {
            change1();
            finish();
        } else if (id == R.id.nav_crew) {
            change2();
            finish();
        } else if(id == R.id.nav_home){
            change3();
            finish();
        } else if(id == R.id.nav_roadster){
            change4();
            finish();
        }else if(id == R.id.nav_launch){
            change5();
        }
        else if(id == R.id.nav_data){
            change6();
            finish();
        }else if(id == R.id.nav_logout){
            change7();
            finish();
        }else if(id == R.id.nav_phasemoon){
            change8();
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void change1(){
        Intent intent  = new Intent(this, IssPositionActivity.class);
        startActivity(intent);
        //Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }
    public void change2(){
        Intent intent  = new Intent(this, IssCrewActivity.class);
        startActivity(intent);
       // Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }

    public void change3(){
        Intent intent = new  Intent(this, MenuActivity.class);
        startActivity(intent);
        //Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }

    public void change4(){
        Intent intent = new  Intent(this, RoadsterInfosActivity.class);
        startActivity(intent);
        //Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }

    public void change5(){
        /*Intent intent = new Intent(this, NextLaunchActivity.class);
        startActivity(intent);*/
        Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }

    public void change6(){
        Intent intent = new Intent(this, CompDataActivity.class);
        startActivity(intent);
        //Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }
    public void change7(){
        Intent intent = new Intent(this, ConnexionActivity.class);
        startActivity(intent);
    }
    public void change8(){
        Intent intent = new Intent(this, PhaseMoonActivity.class);
        startActivity(intent);
    }
}
