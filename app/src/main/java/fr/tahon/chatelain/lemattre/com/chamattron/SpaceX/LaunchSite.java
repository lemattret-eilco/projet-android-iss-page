package fr.tahon.chatelain.lemattre.com.chamattron.SpaceX;

import java.util.HashMap;
import java.util.Map;

public class LaunchSite {

    private String site_id;
    private String site_name;
    private String site_name_ong;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getSiteId() {
        return site_id;
    }

    public void setSiteId(String siteId) {
        this.site_id = siteId;
    }

    public String getSiteName() {
        return site_name;
    }

    public void setSiteName(String siteName) {
        this.site_name = siteName;
    }

    public String getSiteNameLong() {
        return site_name_ong;
    }

    public void setSiteNameLong(String siteNameLong) {
        this.site_name_ong = siteNameLong;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}