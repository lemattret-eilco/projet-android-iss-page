package fr.tahon.chatelain.lemattre.com.chamattron.Activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import fr.tahon.chatelain.lemattre.com.chamattron.Iss.Example;
import fr.tahon.chatelain.lemattre.com.chamattron.R;
import fr.tahon.chatelain.lemattre.com.chamattron.RetrofitService.RetrofitService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static fr.tahon.chatelain.lemattre.com.chamattron.RetrofitService.RetrofitService.ENDPOINT;

public class IssPositionActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private TextView timestampn;
    private TextView latituden;
    private TextView longituden;
    private TextView titre;
    private WebView animation;
    String baseURL = "http://api.open-notify.org/iss-now.json";
    private DrawerLayout mDrawer;
    private NavigationView navigationView;


    private Handler myHandler;
    private Runnable myRunnable = new Runnable() {
        @Override
        public void run() {
            // Code à exécuter de façon périodique
            timestampn = (TextView) findViewById(R.id.timestamp);
            latituden = (TextView) findViewById(R.id.latitude);
            longituden = (TextView) findViewById(R.id.longitude);


            //new JsonTask().execute("Url address here");

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ENDPOINT)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            retrofit.create(RetrofitService.class);

            RetrofitService service = retrofit.create(RetrofitService.class);


            service.listExample().enqueue(new Callback<Example>() {
                @Override
                public void onResponse(Call<Example> call, Response<Example> response) {
                    //Toast.makeText(IssPositionActivity.this, "latitude : " + response.body().getIssPosition().getLatitude(), Toast.LENGTH_SHORT).show();
                    latituden.setText(response.body().getIssPosition().getLatitude());
                    longituden.setText(response.body().getIssPosition().getLongitude());
                    timestampn.setText(response.body().getTimestamp());
                }

                @Override
                public void onFailure(Call<Example> call, Throwable t) {
                    Toast.makeText(IssPositionActivity.this, "ERROR", Toast.LENGTH_SHORT).show();
                }
            });


            myHandler.postDelayed(this,1500);
        }
    };




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iss);
        Toolbar toolbar2 = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar2);
        getSupportActionBar().setTitle("Position Iss");
        myHandler = new Handler();
        myHandler.postDelayed(myRunnable,500);

        titre = (TextView) findViewById(R.id.ISSPositionTitle);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "ARDELANEY.ttf");
        titre.setTypeface(typeface);

        animation = (WebView) findViewById(R.id.isspositionAnimation);
        animation.loadUrl("http://android.busin.fr/iss.html");
        animation.getSettings().setAllowFileAccess(true);
        animation.getSettings().setBlockNetworkImage(false);
        animation.getSettings().setJavaScriptEnabled(true);
        animation.getSettings().setLoadsImagesAutomatically(true);

        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar2, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }


    public void onPause() {
        super.onPause();
        if(myHandler != null)
            myHandler.removeCallbacks(myRunnable); // On arrete le callback
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();

        if (id == R.id.nav_pos) {
            change1();
        } else if (id == R.id.nav_crew) {
            change2();
            finish();
        } else if (id == R.id.nav_home){
            change3();
            finish();
        }else if(id == R.id.nav_roadster){
            change4();
            finish();
        }
        else if(id == R.id.nav_launch){
            change5();
            finish();
        } else if(id == R.id.nav_data){
            change6();
            finish();
        }else if(id == R.id.nav_logout){
            change7();
            finish();
        }else if(id == R.id.nav_phasemoon){
            change8();
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void change1(){
        /*Intent intent  = new Intent(this, IssPositionActivity.class);
        startActivity(intent);*/
        Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }
    public void change2(){
        Intent intent  = new Intent(this, IssCrewActivity.class);
        startActivity(intent);
    }

    public void change3(){
        Intent intent = new  Intent(this, MenuActivity.class);
        startActivity(intent);
        //Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }

    public void change4(){
        Intent intent = new  Intent(this, RoadsterInfosActivity.class);
        startActivity(intent);
        //Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }

    public void change5(){
        Intent intent = new Intent(this, NextLaunchActivity.class);
        startActivity(intent);
        //Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }

    public void change6(){
        Intent intent = new Intent(this, CompDataActivity.class);
        startActivity(intent);
        //Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }
    public void change7(){
        Intent intent = new Intent(this, ConnexionActivity.class);
        startActivity(intent);
    }
    public void change8(){
        Intent intent = new Intent(this, PhaseMoonActivity.class);
        startActivity(intent);
    }


}
