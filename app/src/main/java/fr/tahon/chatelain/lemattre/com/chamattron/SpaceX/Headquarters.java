package fr.tahon.chatelain.lemattre.com.chamattron.SpaceX;

import java.util.HashMap;
import java.util.Map;

public class Headquarters {

    private String address;
    private String city;
    private String state;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}