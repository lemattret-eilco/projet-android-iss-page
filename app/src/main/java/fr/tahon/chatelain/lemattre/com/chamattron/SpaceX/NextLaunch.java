package fr.tahon.chatelain.lemattre.com.chamattron.SpaceX;

public class NextLaunch {

    private Integer flight_number;
    private String mission_name;

    private String launch_date_utc;

    private fr.tahon.chatelain.lemattre.com.chamattron.SpaceX.Rocket rocket;

    private fr.tahon.chatelain.lemattre.com.chamattron.SpaceX.LaunchSite launch_site;


    private fr.tahon.chatelain.lemattre.com.chamattron.SpaceX.Links links;


    public Integer getFlightNumber() {
        return flight_number;
    }

    public void setFlightNumber(Integer flightNumber) {
        this.flight_number = flightNumber;
    }

    public String getMissionName() {
        return mission_name;
    }

    public void setMissionName(String missionName) {
        this.mission_name = missionName;
    }


    public String getLaunchDateUtc() {
        return launch_date_utc;
    }

    public void setLaunchDateUtc(String launchDateUtc) {
        this.launch_date_utc = launchDateUtc;
    }



    public Rocket getRocket() {
        return rocket;
    }

    public void setRocket(Rocket Rocket) {
        this.rocket = Rocket;
    }



    public LaunchSite getLaunchSite() {
        return launch_site;
    }

    public void setLaunchSite(LaunchSite launchSite) {
        this.launch_site = launchSite;
    }


    public Links getLinks() {
        return links;
    }



}