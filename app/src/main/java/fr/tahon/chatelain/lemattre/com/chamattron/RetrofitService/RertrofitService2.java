package fr.tahon.chatelain.lemattre.com.chamattron.RetrofitService;

import fr.tahon.chatelain.lemattre.com.chamattron.SpaceX.InfosComp;
import fr.tahon.chatelain.lemattre.com.chamattron.SpaceX.NextLaunch;
import fr.tahon.chatelain.lemattre.com.chamattron.SpaceX.Roadster;
import retrofit2.Call;
import retrofit2.http.GET;

public interface RertrofitService2 {
    public static final String ENDPOINT = "https://api.spacexdata.com/v3/";

    @GET("roadster")
    Call<Roadster> listExample();

    @GET("launches/next")
    Call<NextLaunch> listExample2();

    @GET("info")
    Call<InfosComp> listExample3();
}
