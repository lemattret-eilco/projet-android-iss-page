package fr.tahon.chatelain.lemattre.com.chamattron.Activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;
import com.bumptech.glide.Glide;

import fr.tahon.chatelain.lemattre.com.chamattron.R;
import fr.tahon.chatelain.lemattre.com.chamattron.RetrofitService.RertrofitService2;
import fr.tahon.chatelain.lemattre.com.chamattron.SpaceX.Roadster;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static fr.tahon.chatelain.lemattre.com.chamattron.RetrofitService.RertrofitService2.ENDPOINT;

public class RoadsterInfosActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private TextView date;
    private TextView masse;
    private TextView orbite;
    private TextView vitesse;
    private TextView distanceT;
    private TextView distanceM;
    private TextView titre;
    private ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_roadster_infos);


        titre = (TextView) findViewById(R.id.RTitle);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "ARDELANEY.ttf");
        titre.setTypeface(typeface);
        date = (TextView) findViewById(R.id.RL1t);
        masse = (TextView) findViewById(R.id.RL2t);
        orbite = (TextView) findViewById(R.id.RL3t);
        vitesse = (TextView) findViewById(R.id.RL4t);
        distanceT = (TextView) findViewById(R.id.RL5t);
        distanceM = (TextView) findViewById(R.id.RL6t);
        img = (ImageView) findViewById(R.id.image);
        Toolbar toolbar4 = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar4);
        getSupportActionBar().setTitle("Roadster infos");

        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar4, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        retrofit.create(RertrofitService2.class);

        RertrofitService2 service = retrofit.create(RertrofitService2.class);


        service.listExample().enqueue(new Callback<Roadster>() {
            @Override
            public void onResponse(Call<Roadster> call, Response<Roadster> response) {
                date.setText(response.body().getLaunchDateUtc());
                masse.setText(response.body().getLaunchMassKg().toString());
                orbite.setText(response.body().getOrbitType().toString());
                vitesse.setText(response.body().getSpeedMph().toString());
                distanceT.setText(response.body().getEarthDistanceKm().toString());
                distanceM.setText(response.body().getMarsDistanceKm().toString());
                //img.setI(response.body().getPeople().get(5).getName().toString());
                Glide.with(RoadsterInfosActivity.this).load(response.body().getFlickrImages().get(0)).into(img);

            }

            @Override
            public void onFailure(Call<Roadster> call, Throwable t) {
                Toast.makeText(RoadsterInfosActivity.this, "ERROR", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();

        if (id == R.id.nav_pos) {
            change1();
            finish();
        } else if (id == R.id.nav_crew) {
            change2();
            finish();
        } else if(id == R.id.nav_home){
            change3();
            finish();
        } else if(id == R.id.nav_roadster){
            change4();
        }else if(id == R.id.nav_launch){
            change5();
            finish();
        }else if(id == R.id.nav_data){
            change6();
            finish();
        } else if (id == R.id.nav_logout){
            change7();
            finish();
        } else if (id == R.id.nav_phasemoon){
            change8();
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void change1(){
        Intent intent  = new Intent(this, IssPositionActivity.class);
        startActivity(intent);
        //Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }
    public void change2(){
        Intent intent  = new Intent(this, IssCrewActivity.class);
        startActivity(intent);
    }

    public void change3(){
        Intent intent = new  Intent(this, MenuActivity.class);
        startActivity(intent);
        //Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }

    public void change4(){
        /*Intent intent = new  Intent(this, RoadsterInfosActivity.class);
        startActivity(intent);*/
        Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }

    public void change5(){
        Intent intent = new  Intent(this, NextLaunchActivity.class);
        startActivity(intent);
        //Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }

    public void change6(){
        Intent intent = new  Intent(this, CompDataActivity.class);
        startActivity(intent);
        //Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }
    public void change7(){
        Intent intent = new Intent(this, ConnexionActivity.class);
        startActivity(intent);
    }
    public void change8(){
        Intent intent = new Intent(this, PhaseMoonActivity.class);
        startActivity(intent);
    }
}
