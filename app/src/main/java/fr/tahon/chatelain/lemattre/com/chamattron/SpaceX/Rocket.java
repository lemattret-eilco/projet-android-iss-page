package fr.tahon.chatelain.lemattre.com.chamattron.SpaceX;

import java.util.HashMap;
import java.util.Map;

public class Rocket {

    private String rocket_id;
    private String rocket_name;
    private String rocket_type;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getRocketId() {
        return rocket_id;
    }

    public void setRocketId(String rocketId) {
        this.rocket_id = rocketId;
    }

    public String getRocketName() {
        return rocket_name;
    }

    public void setRocketName(String rocketName) {
        this.rocket_name = rocketName;
    }

    public String getRocketType() {
        return rocket_type;
    }

    public void setRocketType(String rocketType) {
        this.rocket_type = rocketType;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}