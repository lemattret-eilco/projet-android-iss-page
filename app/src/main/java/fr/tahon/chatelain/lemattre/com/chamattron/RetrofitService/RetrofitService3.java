package fr.tahon.chatelain.lemattre.com.chamattron.RetrofitService;

import fr.tahon.chatelain.lemattre.com.chamattron.PhaseMoon.Moon;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RetrofitService3 {

    public static final String ENDPOINT = "https://weather.api.here.com/weather/1.0/";

    @GET("report.{format}")
    Call<Moon> listExample(
            @Path("format") String format,
            @Query("app_id") String app_id,
            @Query("app_code") String app_code,
            @Query("product") String product,
            @Query("name") String name


    );
    /*format = "json"
    app_id = "W4tghLWiJWarfZboFOFS"
    app_code = "Yzbsi_Vm7_UGFTogsRa65Q"
    product = "forecast_astronomy"
    name = "Calais"*/




}
