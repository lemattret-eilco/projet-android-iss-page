package fr.tahon.chatelain.lemattre.com.chamattron.RetrofitService;

import fr.tahon.chatelain.lemattre.com.chamattron.Iss.Crew;
import fr.tahon.chatelain.lemattre.com.chamattron.Iss.Example;
import retrofit2.Call;
import retrofit2.http.GET;

public interface RetrofitService {
    public static final String ENDPOINT = "http://api.open-notify.org";

    @GET("/iss-now.json")
    Call<Example> listExample();

    @GET("/astros.json")
    Call<Crew> listExample2();
}
