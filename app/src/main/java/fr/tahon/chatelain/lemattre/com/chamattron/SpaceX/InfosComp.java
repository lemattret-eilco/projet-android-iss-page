package fr.tahon.chatelain.lemattre.com.chamattron.SpaceX;

import java.util.HashMap;
import java.util.Map;

import fr.tahon.chatelain.lemattre.com.chamattron.SpaceX.Headquarters;

public class InfosComp {

    private String name;
    private String founder;
    private Integer founded;
    private Integer employees;
    private Integer vehicles;
    private Integer launch_sites;
    private Integer test_sites;
    private String ceo;
    private String cto;
    private String coo;
    private String cto_propulsion;
    private fr.tahon.chatelain.lemattre.com.chamattron.SpaceX.Headquarters headquarters;
    private String summary;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFounder() {
        return founder;
    }

    public void setFounder(String founder) {
        this.founder = founder;
    }

    public Integer getFounded() {
        return founded;
    }

    public void setFounded(Integer founded) {
        this.founded = founded;
    }

    public Integer getEmployees() {
        return employees;
    }

    public void setEmployees(Integer employees) {
        this.employees = employees;
    }

    public Integer getVehicles() {
        return vehicles;
    }

    public void setVehicles(Integer vehicles) {
        this.vehicles = vehicles;
    }

    public Integer getLaunchSites() {
        return launch_sites;
    }

    public void setLaunchSites(Integer launchSites) {
        this.launch_sites = launchSites;
    }

    public Integer getTestSites() {
        return test_sites;
    }

    public void setTestSites(Integer testSites) {
        this.test_sites = testSites;
    }

    public String getCeo() {
        return ceo;
    }

    public void setCeo(String ceo) {
        this.ceo = ceo;
    }

    public String getCto() {
        return cto;
    }

    public void setCto(String cto) {
        this.cto = cto;
    }

    public String getCoo() {
        return coo;
    }

    public void setCoo(String coo) {
        this.coo = coo;
    }

    public String getCtoPropulsion() {
        return cto_propulsion;
    }

    public void setCtoPropulsion(String ctoPropulsion) {
        this.cto_propulsion = ctoPropulsion;
    }

    public Headquarters getHeadquarters() {
        return headquarters;
    }

    public void setHeadquarters(Headquarters Headquarters) {
        this.headquarters = Headquarters;
    }


    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}