package fr.tahon.chatelain.lemattre.com.chamattron.Activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import fr.tahon.chatelain.lemattre.com.chamattron.R;

public class MainActivity extends AppCompatActivity {

    private static final  int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent homeIntent = new Intent(MainActivity.this, ConnexionActivity.class);
            startActivity(homeIntent);
            finish();
            }
        },SPLASH_TIME_OUT);
    }
}
