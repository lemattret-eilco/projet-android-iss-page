package fr.tahon.chatelain.lemattre.com.chamattron.PhaseMoon;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Astronomy {

    private List<Astronomy_> astronomy = null;
    private String country;
    private String state;
    private String city;
    private Double latitude;
    private Double longitude;
    private Integer timezone;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public List<Astronomy_> getAstronomy() {
        return astronomy;
    }

    public void setAstronomy(List<Astronomy_> astronomy) {
        this.astronomy = astronomy;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getTimezone() {
        return timezone;
    }

    public void setTimezone(Integer timezone) {
        this.timezone = timezone;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
