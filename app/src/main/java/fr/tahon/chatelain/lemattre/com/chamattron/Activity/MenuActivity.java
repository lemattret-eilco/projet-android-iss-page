package fr.tahon.chatelain.lemattre.com.chamattron.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import fr.tahon.chatelain.lemattre.com.chamattron.R;

public class MenuActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private TextView hello;
    private TextView presentation;
    private TextView auteurs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Accueil");
        hello = (TextView) findViewById(R.id.HelloTitle);
        presentation = (TextView) findViewById(R.id.presentation);
        auteurs = (TextView) findViewById(R.id.auteurs);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent Email = new Intent(Intent.ACTION_SEND);
                Email.setType("text/email");
                Email.putExtra(Intent.EXTRA_EMAIL, new String[]{"remi.tahon@eilco-ulco.fr;thibault.lemattre.elv@eilco-ulco.fr;pierre.chatelain.elv@eilco-ulco.fr"});
                Email.putExtra(Intent.EXTRA_SUBJECT,"Add your subject");
                Email.putExtra(Intent.EXTRA_TEXT,"Messieurs les développeurs, "+"");
                startActivity(Intent.createChooser(Email, "send feedback : "));
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the MenuActivity; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        if (id == R.id.nav_pos) {
           change1();
           finish();
        } else if (id == R.id.nav_crew) {
            change2();
            finish();
        }
        else if (id== R.id.nav_home){
            change3();
        }else if (id== R.id.nav_roadster){
            change4();
            finish();
        }
        else if (id== R.id.nav_launch){
            change5();
            finish();
        }else if (id== R.id.nav_data){
            change6();
            finish();
        } else if (id == R.id.nav_logout){
            change7();
            finish();
        }else if (id == R.id.nav_phasemoon){
            change8();
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void change1(){
        Intent intent  = new Intent(this, IssPositionActivity.class);
        startActivity(intent);
    }
    public void change2(){
        Intent intent  = new Intent(this, IssCrewActivity.class);
        startActivity(intent);
    }

    public void change3(){
        Toast.makeText(getApplicationContext(), "Déjà sur la page ", Toast.LENGTH_SHORT).show();
    }

    public void change4(){
        Intent intent = new Intent(this, RoadsterInfosActivity.class);
        startActivity(intent);
    }

    public void change5(){
        Intent intent = new Intent(this, NextLaunchActivity.class);
        startActivity(intent);
    }

    public void change6(){
        Intent intent = new Intent(this, CompDataActivity.class);
        startActivity(intent);
    }

    public void change7(){
        Intent intent = new Intent(this, ConnexionActivity.class);
        startActivity(intent);
    }

    public void change8(){
        Intent intent = new Intent(this, PhaseMoonActivity.class);
        startActivity(intent);
    }
}
