package fr.tahon.chatelain.lemattre.com.chamattron.Activity;


import android.content.Intent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import fr.tahon.chatelain.lemattre.com.chamattron.R;

public class InscritpionActivity extends AppCompatActivity {


    private EditText mail;
    private EditText mdp1;
    private EditText mdp2;
    private Button valider;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription_activity);
        this.mail = (EditText) findViewById(R.id.id_inscription);
        this.mdp1 = (EditText) findViewById(R.id.mdp1);
        this.mdp2 = (EditText) findViewById(R.id.mdp2);
        this.valider = (Button) findViewById(R.id.valider_inscription);

        //mAuth = FirebaseAuth.getInstance();


        this.valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enregistrement();

            }
        });
    }

    public void enregistrement() {
        String mdp_1 = this.mdp1.getText().toString();
        String mdp_2 = this.mdp2.getText().toString();
        String mail = this.mail.getText().toString();
        if (!mdp_1.isEmpty() && !mdp_2.isEmpty() && !mail.isEmpty()) {
            if (mdp_1.equals(mdp_2)) {
                change();

            } else {
                Toast.makeText(getApplicationContext(), "Passwords aren't equals !", Toast.LENGTH_SHORT).show();
                return;

            }
        } else {
            Toast.makeText(getApplicationContext(), "Please fill all labels !", Toast.LENGTH_SHORT).show();
            return;
        }
    }


    public void change(){
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
    }
}


