package fr.tahon.chatelain.lemattre.com.chamattron.PhaseMoon;

import java.util.HashMap;
import java.util.Map;

import fr.tahon.chatelain.lemattre.com.chamattron.PhaseMoon.Astronomy;

public class Moon {
    private Astronomy astronomy;
    private String feedCreation;
    private Boolean metric;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Astronomy getAstronomy() {
        return astronomy;
    }

    public void setAstronomy(Astronomy astronomy) {
        this.astronomy = astronomy;
    }

    public String getFeedCreation() {
        return feedCreation;
    }

    public void setFeedCreation(String feedCreation) {
        this.feedCreation = feedCreation;
    }

    public Boolean getMetric() {
        return metric;
    }

    public void setMetric(Boolean metric) {
        this.metric = metric;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
