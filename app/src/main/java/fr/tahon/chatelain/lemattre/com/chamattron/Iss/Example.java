
package fr.tahon.chatelain.lemattre.com.chamattron.Iss;

import java.util.HashMap;
import java.util.Map;

import fr.tahon.chatelain.lemattre.com.chamattron.Iss.IssPosition;

public class Example {

    private IssPosition iss_position;
    private String message;
    private String timestamp;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public IssPosition getIssPosition() {
        return iss_position;
    }

    public void setIssPosition(IssPosition issPosition) {
        this.iss_position = issPosition;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}